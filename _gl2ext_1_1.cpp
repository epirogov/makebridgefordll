void _gl2ext_1_1::_glBlendBarrierKHR (void);
{
	glBlendBarrierKHR (void);
}

void _gl2ext_1_1::_glDebugMessageControlKHR (GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled);
{
	glDebugMessageControlKHR (GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled);
}

void _gl2ext_1_1::_glDebugMessageInsertKHR (GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf);
{
	glDebugMessageInsertKHR (GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *buf);
}

void _gl2ext_1_1::_glDebugMessageCallbackKHR (GLDEBUGPROCKHR callback, const void *userParam);
{
	glDebugMessageCallbackKHR (GLDEBUGPROCKHR callback, const void *userParam);
}

GLuint _gl2ext_1_1::_glGetDebugMessageLogKHR (GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog);
{
	return glGetDebugMessageLogKHR (GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog);
}

void _gl2ext_1_1::_glPushDebugGroupKHR (GLenum source, GLuint id, GLsizei length, const GLchar *message);
{
	glPushDebugGroupKHR (GLenum source, GLuint id, GLsizei length, const GLchar *message);
}

void _gl2ext_1_1::_glPopDebugGroupKHR (void);
{
	glPopDebugGroupKHR (void);
}

void _gl2ext_1_1::_glObjectLabelKHR (GLenum identifier, GLuint name, GLsizei length, const GLchar *label);
{
	glObjectLabelKHR (GLenum identifier, GLuint name, GLsizei length, const GLchar *label);
}

void _gl2ext_1_1::_glGetObjectLabelKHR (GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label);
{
	glGetObjectLabelKHR (GLenum identifier, GLuint name, GLsizei bufSize, GLsizei *length, GLchar *label);
}

void _gl2ext_1_1::_glObjectPtrLabelKHR (const void *ptr, GLsizei length, const GLchar *label);
{
	glObjectPtrLabelKHR (const void *ptr, GLsizei length, const GLchar *label);
}

void _gl2ext_1_1::_glGetObjectPtrLabelKHR (const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label);
{
	glGetObjectPtrLabelKHR (const void *ptr, GLsizei bufSize, GLsizei *length, GLchar *label);
}

void _gl2ext_1_1::_glGetPointervKHR (GLenum pname, void **params);
{
	glGetPointervKHR (GLenum pname, void **params);
}

GLenum _gl2ext_1_1::_glGetGraphicsResetStatusKHR (void);
{
	return glGetGraphicsResetStatusKHR (void);
}

void _gl2ext_1_1::_glReadnPixelsKHR (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data);
{
	glReadnPixelsKHR (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data);
}

void _gl2ext_1_1::_glGetnUniformfvKHR (GLuint program, GLint location, GLsizei bufSize, GLfloat *params);
{
	glGetnUniformfvKHR (GLuint program, GLint location, GLsizei bufSize, GLfloat *params);
}

void _gl2ext_1_1::_glGetnUniformivKHR (GLuint program, GLint location, GLsizei bufSize, GLint *params);
{
	glGetnUniformivKHR (GLuint program, GLint location, GLsizei bufSize, GLint *params);
}

void _gl2ext_1_1::_glGetnUniformuivKHR (GLuint program, GLint location, GLsizei bufSize, GLuint *params);
{
	glGetnUniformuivKHR (GLuint program, GLint location, GLsizei bufSize, GLuint *params);
}

void _gl2ext_1_1::_glEGLImageTargetTexture2DOES (GLenum target, GLeglImageOES image);
{
	glEGLImageTargetTexture2DOES (GLenum target, GLeglImageOES image);
}

void _gl2ext_1_1::_glEGLImageTargetRenderbufferStorageOES (GLenum target, GLeglImageOES image);
{
	glEGLImageTargetRenderbufferStorageOES (GLenum target, GLeglImageOES image);
}

void _gl2ext_1_1::_glCopyImageSubDataOES (GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth);
{
	glCopyImageSubDataOES (GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth);
}

void _gl2ext_1_1::_glEnableiOES (GLenum target, GLuint index);
{
	glEnableiOES (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glDisableiOES (GLenum target, GLuint index);
{
	glDisableiOES (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glBlendEquationiOES (GLuint buf, GLenum mode);
{
	glBlendEquationiOES (GLuint buf, GLenum mode);
}

void _gl2ext_1_1::_glBlendEquationSeparateiOES (GLuint buf, GLenum modeRGB, GLenum modeAlpha);
{
	glBlendEquationSeparateiOES (GLuint buf, GLenum modeRGB, GLenum modeAlpha);
}

void _gl2ext_1_1::_glBlendFunciOES (GLuint buf, GLenum src, GLenum dst);
{
	glBlendFunciOES (GLuint buf, GLenum src, GLenum dst);
}

void _gl2ext_1_1::_glBlendFuncSeparateiOES (GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha);
{
	glBlendFuncSeparateiOES (GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha);
}

void _gl2ext_1_1::_glColorMaskiOES (GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a);
{
	glColorMaskiOES (GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a);
}

GLboolean _gl2ext_1_1::_glIsEnablediOES (GLenum target, GLuint index);
{
	return glIsEnablediOES (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glDrawElementsBaseVertexOES (GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex);
{
	glDrawElementsBaseVertexOES (GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex);
}

void _gl2ext_1_1::_glDrawRangeElementsBaseVertexOES (GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex);
{
	glDrawRangeElementsBaseVertexOES (GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex);
}

void _gl2ext_1_1::_glDrawElementsInstancedBaseVertexOES (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex);
{
	glDrawElementsInstancedBaseVertexOES (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex);
}

void _gl2ext_1_1::_glMultiDrawElementsBaseVertexOES (GLenum mode, const GLsizei *count, GLenum type, const void *const*indices, GLsizei primcount, const GLint *basevertex);
{
	glMultiDrawElementsBaseVertexOES (GLenum mode, const GLsizei *count, GLenum type, const void *const*indices, GLsizei primcount, const GLint *basevertex);
}

void _gl2ext_1_1::_glFramebufferTextureOES (GLenum target, GLenum attachment, GLuint texture, GLint level);
{
	glFramebufferTextureOES (GLenum target, GLenum attachment, GLuint texture, GLint level);
}

void _gl2ext_1_1::_glGetProgramBinaryOES (GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary);
{
	glGetProgramBinaryOES (GLuint program, GLsizei bufSize, GLsizei *length, GLenum *binaryFormat, void *binary);
}

void _gl2ext_1_1::_glProgramBinaryOES (GLuint program, GLenum binaryFormat, const void *binary, GLint length);
{
	glProgramBinaryOES (GLuint program, GLenum binaryFormat, const void *binary, GLint length);
}

void _gl2ext_1_1::_glMapBufferOES (GLenum target, GLenum access);
{
	glMapBufferOES (GLenum target, GLenum access);
}

GLboolean _gl2ext_1_1::_glUnmapBufferOES (GLenum target);
{
	return glUnmapBufferOES (GLenum target);
}

void _gl2ext_1_1::_glGetBufferPointervOES (GLenum target, GLenum pname, void **params);
{
	glGetBufferPointervOES (GLenum target, GLenum pname, void **params);
}

void _gl2ext_1_1::_glPrimitiveBoundingBoxOES (GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW);
{
	glPrimitiveBoundingBoxOES (GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW);
}

void _gl2ext_1_1::_glMinSampleShadingOES (GLfloat value);
{
	glMinSampleShadingOES (GLfloat value);
}

void _gl2ext_1_1::_glPatchParameteriOES (GLenum pname, GLint value);
{
	glPatchParameteriOES (GLenum pname, GLint value);
}

void _gl2ext_1_1::_glTexImage3DOES (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels);
{
	glTexImage3DOES (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const void *pixels);
}

void _gl2ext_1_1::_glTexSubImage3DOES (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels);
{
	glTexSubImage3DOES (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels);
}

void _gl2ext_1_1::_glCopyTexSubImage3DOES (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height);
{
	glCopyTexSubImage3DOES (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glCompressedTexImage3DOES (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data);
{
	glCompressedTexImage3DOES (GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const void *data);
}

void _gl2ext_1_1::_glCompressedTexSubImage3DOES (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data);
{
	glCompressedTexSubImage3DOES (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const void *data);
}

void _gl2ext_1_1::_glFramebufferTexture3DOES (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset);
{
	glFramebufferTexture3DOES (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint zoffset);
}

void _gl2ext_1_1::_glTexParameterIivOES (GLenum target, GLenum pname, const GLint *params);
{
	glTexParameterIivOES (GLenum target, GLenum pname, const GLint *params);
}

void _gl2ext_1_1::_glTexParameterIuivOES (GLenum target, GLenum pname, const GLuint *params);
{
	glTexParameterIuivOES (GLenum target, GLenum pname, const GLuint *params);
}

void _gl2ext_1_1::_glGetTexParameterIivOES (GLenum target, GLenum pname, GLint *params);
{
	glGetTexParameterIivOES (GLenum target, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glGetTexParameterIuivOES (GLenum target, GLenum pname, GLuint *params);
{
	glGetTexParameterIuivOES (GLenum target, GLenum pname, GLuint *params);
}

void _gl2ext_1_1::_glSamplerParameterIivOES (GLuint sampler, GLenum pname, const GLint *param);
{
	glSamplerParameterIivOES (GLuint sampler, GLenum pname, const GLint *param);
}

void _gl2ext_1_1::_glSamplerParameterIuivOES (GLuint sampler, GLenum pname, const GLuint *param);
{
	glSamplerParameterIuivOES (GLuint sampler, GLenum pname, const GLuint *param);
}

void _gl2ext_1_1::_glGetSamplerParameterIivOES (GLuint sampler, GLenum pname, GLint *params);
{
	glGetSamplerParameterIivOES (GLuint sampler, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glGetSamplerParameterIuivOES (GLuint sampler, GLenum pname, GLuint *params);
{
	glGetSamplerParameterIuivOES (GLuint sampler, GLenum pname, GLuint *params);
}

void _gl2ext_1_1::_glTexBufferOES (GLenum target, GLenum internalformat, GLuint buffer);
{
	glTexBufferOES (GLenum target, GLenum internalformat, GLuint buffer);
}

void _gl2ext_1_1::_glTexBufferRangeOES (GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size);
{
	glTexBufferRangeOES (GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size);
}

void _gl2ext_1_1::_glTexStorage3DMultisampleOES (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations);
{
	glTexStorage3DMultisampleOES (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations);
}

void _gl2ext_1_1::_glTextureViewOES (GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers);
{
	glTextureViewOES (GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers);
}

void _gl2ext_1_1::_glBindVertexArrayOES (GLuint array);
{
	glBindVertexArrayOES (GLuint array);
}

void _gl2ext_1_1::_glDeleteVertexArraysOES (GLsizei n, const GLuint *arrays);
{
	glDeleteVertexArraysOES (GLsizei n, const GLuint *arrays);
}

void _gl2ext_1_1::_glGenVertexArraysOES (GLsizei n, GLuint *arrays);
{
	glGenVertexArraysOES (GLsizei n, GLuint *arrays);
}

GLboolean _gl2ext_1_1::_glIsVertexArrayOES (GLuint array);
{
	return glIsVertexArrayOES (GLuint array);
}

void _gl2ext_1_1::_glGetPerfMonitorGroupsAMD (GLint *numGroups, GLsizei groupsSize, GLuint *groups);
{
	glGetPerfMonitorGroupsAMD (GLint *numGroups, GLsizei groupsSize, GLuint *groups);
}

void _gl2ext_1_1::_glGetPerfMonitorCountersAMD (GLuint group, GLint *numCounters, GLint *maxActiveCounters, GLsizei counterSize, GLuint *counters);
{
	glGetPerfMonitorCountersAMD (GLuint group, GLint *numCounters, GLint *maxActiveCounters, GLsizei counterSize, GLuint *counters);
}

void _gl2ext_1_1::_glGetPerfMonitorGroupStringAMD (GLuint group, GLsizei bufSize, GLsizei *length, GLchar *groupString);
{
	glGetPerfMonitorGroupStringAMD (GLuint group, GLsizei bufSize, GLsizei *length, GLchar *groupString);
}

void _gl2ext_1_1::_glGetPerfMonitorCounterStringAMD (GLuint group, GLuint counter, GLsizei bufSize, GLsizei *length, GLchar *counterString);
{
	glGetPerfMonitorCounterStringAMD (GLuint group, GLuint counter, GLsizei bufSize, GLsizei *length, GLchar *counterString);
}

void _gl2ext_1_1::_glGetPerfMonitorCounterInfoAMD (GLuint group, GLuint counter, GLenum pname, void *data);
{
	glGetPerfMonitorCounterInfoAMD (GLuint group, GLuint counter, GLenum pname, void *data);
}

void _gl2ext_1_1::_glGenPerfMonitorsAMD (GLsizei n, GLuint *monitors);
{
	glGenPerfMonitorsAMD (GLsizei n, GLuint *monitors);
}

void _gl2ext_1_1::_glDeletePerfMonitorsAMD (GLsizei n, GLuint *monitors);
{
	glDeletePerfMonitorsAMD (GLsizei n, GLuint *monitors);
}

void _gl2ext_1_1::_glSelectPerfMonitorCountersAMD (GLuint monitor, GLboolean enable, GLuint group, GLint numCounters, GLuint *counterList);
{
	glSelectPerfMonitorCountersAMD (GLuint monitor, GLboolean enable, GLuint group, GLint numCounters, GLuint *counterList);
}

void _gl2ext_1_1::_glBeginPerfMonitorAMD (GLuint monitor);
{
	glBeginPerfMonitorAMD (GLuint monitor);
}

void _gl2ext_1_1::_glEndPerfMonitorAMD (GLuint monitor);
{
	glEndPerfMonitorAMD (GLuint monitor);
}

void _gl2ext_1_1::_glGetPerfMonitorCounterDataAMD (GLuint monitor, GLenum pname, GLsizei dataSize, GLuint *data, GLint *bytesWritten);
{
	glGetPerfMonitorCounterDataAMD (GLuint monitor, GLenum pname, GLsizei dataSize, GLuint *data, GLint *bytesWritten);
}

void _gl2ext_1_1::_glBlitFramebufferANGLE (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
{
	glBlitFramebufferANGLE (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
}

void _gl2ext_1_1::_glRenderbufferStorageMultisampleANGLE (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
{
	glRenderbufferStorageMultisampleANGLE (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glDrawArraysInstancedANGLE (GLenum mode, GLint first, GLsizei count, GLsizei primcount);
{
	glDrawArraysInstancedANGLE (GLenum mode, GLint first, GLsizei count, GLsizei primcount);
}

void _gl2ext_1_1::_glDrawElementsInstancedANGLE (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei primcount);
{
	glDrawElementsInstancedANGLE (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei primcount);
}

void _gl2ext_1_1::_glVertexAttribDivisorANGLE (GLuint index, GLuint divisor);
{
	glVertexAttribDivisorANGLE (GLuint index, GLuint divisor);
}

void _gl2ext_1_1::_glGetTranslatedShaderSourceANGLE (GLuint shader, GLsizei bufsize, GLsizei *length, GLchar *source);
{
	glGetTranslatedShaderSourceANGLE (GLuint shader, GLsizei bufsize, GLsizei *length, GLchar *source);
}

void _gl2ext_1_1::_glCopyTextureLevelsAPPLE (GLuint destinationTexture, GLuint sourceTexture, GLint sourceBaseLevel, GLsizei sourceLevelCount);
{
	glCopyTextureLevelsAPPLE (GLuint destinationTexture, GLuint sourceTexture, GLint sourceBaseLevel, GLsizei sourceLevelCount);
}

void _gl2ext_1_1::_glRenderbufferStorageMultisampleAPPLE (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
{
	glRenderbufferStorageMultisampleAPPLE (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glResolveMultisampleFramebufferAPPLE (void);
{
	glResolveMultisampleFramebufferAPPLE (void);
}

GLsync _gl2ext_1_1::_glFenceSyncAPPLE (GLenum condition, GLbitfield flags);
{
	return glFenceSyncAPPLE (GLenum condition, GLbitfield flags);
}

GLboolean _gl2ext_1_1::_glIsSyncAPPLE (GLsync sync);
{
	return glIsSyncAPPLE (GLsync sync);
}

void _gl2ext_1_1::_glDeleteSyncAPPLE (GLsync sync);
{
	glDeleteSyncAPPLE (GLsync sync);
}

GLenum _gl2ext_1_1::_glClientWaitSyncAPPLE (GLsync sync, GLbitfield flags, GLuint64 timeout);
{
	return glClientWaitSyncAPPLE (GLsync sync, GLbitfield flags, GLuint64 timeout);
}

void _gl2ext_1_1::_glWaitSyncAPPLE (GLsync sync, GLbitfield flags, GLuint64 timeout);
{
	glWaitSyncAPPLE (GLsync sync, GLbitfield flags, GLuint64 timeout);
}

void _gl2ext_1_1::_glGetInteger64vAPPLE (GLenum pname, GLint64 *params);
{
	glGetInteger64vAPPLE (GLenum pname, GLint64 *params);
}

void _gl2ext_1_1::_glGetSyncivAPPLE (GLsync sync, GLenum pname, GLsizei bufSize, GLsizei *length, GLint *values);
{
	glGetSyncivAPPLE (GLsync sync, GLenum pname, GLsizei bufSize, GLsizei *length, GLint *values);
}

void _gl2ext_1_1::_glDrawArraysInstancedBaseInstanceEXT (GLenum mode, GLint first, GLsizei count, GLsizei instancecount, GLuint baseinstance);
{
	glDrawArraysInstancedBaseInstanceEXT (GLenum mode, GLint first, GLsizei count, GLsizei instancecount, GLuint baseinstance);
}

void _gl2ext_1_1::_glDrawElementsInstancedBaseInstanceEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLuint baseinstance);
{
	glDrawElementsInstancedBaseInstanceEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLuint baseinstance);
}

void _gl2ext_1_1::_glDrawElementsInstancedBaseVertexBaseInstanceEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex, GLuint baseinstance);
{
	glDrawElementsInstancedBaseVertexBaseInstanceEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex, GLuint baseinstance);
}

void _gl2ext_1_1::_glCopyImageSubDataEXT (GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth);
{
	glCopyImageSubDataEXT (GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth);
}

void _gl2ext_1_1::_glLabelObjectEXT (GLenum type, GLuint object, GLsizei length, const GLchar *label);
{
	glLabelObjectEXT (GLenum type, GLuint object, GLsizei length, const GLchar *label);
}

void _gl2ext_1_1::_glGetObjectLabelEXT (GLenum type, GLuint object, GLsizei bufSize, GLsizei *length, GLchar *label);
{
	glGetObjectLabelEXT (GLenum type, GLuint object, GLsizei bufSize, GLsizei *length, GLchar *label);
}

void _gl2ext_1_1::_glInsertEventMarkerEXT (GLsizei length, const GLchar *marker);
{
	glInsertEventMarkerEXT (GLsizei length, const GLchar *marker);
}

void _gl2ext_1_1::_glPushGroupMarkerEXT (GLsizei length, const GLchar *marker);
{
	glPushGroupMarkerEXT (GLsizei length, const GLchar *marker);
}

void _gl2ext_1_1::_glPopGroupMarkerEXT (void);
{
	glPopGroupMarkerEXT (void);
}

void _gl2ext_1_1::_glDiscardFramebufferEXT (GLenum target, GLsizei numAttachments, const GLenum *attachments);
{
	glDiscardFramebufferEXT (GLenum target, GLsizei numAttachments, const GLenum *attachments);
}

void _gl2ext_1_1::_glGenQueriesEXT (GLsizei n, GLuint *ids);
{
	glGenQueriesEXT (GLsizei n, GLuint *ids);
}

void _gl2ext_1_1::_glDeleteQueriesEXT (GLsizei n, const GLuint *ids);
{
	glDeleteQueriesEXT (GLsizei n, const GLuint *ids);
}

GLboolean _gl2ext_1_1::_glIsQueryEXT (GLuint id);
{
	return glIsQueryEXT (GLuint id);
}

void _gl2ext_1_1::_glBeginQueryEXT (GLenum target, GLuint id);
{
	glBeginQueryEXT (GLenum target, GLuint id);
}

void _gl2ext_1_1::_glEndQueryEXT (GLenum target);
{
	glEndQueryEXT (GLenum target);
}

void _gl2ext_1_1::_glQueryCounterEXT (GLuint id, GLenum target);
{
	glQueryCounterEXT (GLuint id, GLenum target);
}

void _gl2ext_1_1::_glGetQueryivEXT (GLenum target, GLenum pname, GLint *params);
{
	glGetQueryivEXT (GLenum target, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glGetQueryObjectivEXT (GLuint id, GLenum pname, GLint *params);
{
	glGetQueryObjectivEXT (GLuint id, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glGetQueryObjectuivEXT (GLuint id, GLenum pname, GLuint *params);
{
	glGetQueryObjectuivEXT (GLuint id, GLenum pname, GLuint *params);
}

void _gl2ext_1_1::_glGetQueryObjecti64vEXT (GLuint id, GLenum pname, GLint64 *params);
{
	glGetQueryObjecti64vEXT (GLuint id, GLenum pname, GLint64 *params);
}

void _gl2ext_1_1::_glGetQueryObjectui64vEXT (GLuint id, GLenum pname, GLuint64 *params);
{
	glGetQueryObjectui64vEXT (GLuint id, GLenum pname, GLuint64 *params);
}

void _gl2ext_1_1::_glDrawBuffersEXT (GLsizei n, const GLenum *bufs);
{
	glDrawBuffersEXT (GLsizei n, const GLenum *bufs);
}

void _gl2ext_1_1::_glEnableiEXT (GLenum target, GLuint index);
{
	glEnableiEXT (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glDisableiEXT (GLenum target, GLuint index);
{
	glDisableiEXT (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glBlendEquationiEXT (GLuint buf, GLenum mode);
{
	glBlendEquationiEXT (GLuint buf, GLenum mode);
}

void _gl2ext_1_1::_glBlendEquationSeparateiEXT (GLuint buf, GLenum modeRGB, GLenum modeAlpha);
{
	glBlendEquationSeparateiEXT (GLuint buf, GLenum modeRGB, GLenum modeAlpha);
}

void _gl2ext_1_1::_glBlendFunciEXT (GLuint buf, GLenum src, GLenum dst);
{
	glBlendFunciEXT (GLuint buf, GLenum src, GLenum dst);
}

void _gl2ext_1_1::_glBlendFuncSeparateiEXT (GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha);
{
	glBlendFuncSeparateiEXT (GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha);
}

void _gl2ext_1_1::_glColorMaskiEXT (GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a);
{
	glColorMaskiEXT (GLuint index, GLboolean r, GLboolean g, GLboolean b, GLboolean a);
}

GLboolean _gl2ext_1_1::_glIsEnablediEXT (GLenum target, GLuint index);
{
	return glIsEnablediEXT (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glDrawElementsBaseVertexEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex);
{
	glDrawElementsBaseVertexEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLint basevertex);
}

void _gl2ext_1_1::_glDrawRangeElementsBaseVertexEXT (GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex);
{
	glDrawRangeElementsBaseVertexEXT (GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const void *indices, GLint basevertex);
}

void _gl2ext_1_1::_glDrawElementsInstancedBaseVertexEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex);
{
	glDrawElementsInstancedBaseVertexEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei instancecount, GLint basevertex);
}

void _gl2ext_1_1::_glMultiDrawElementsBaseVertexEXT (GLenum mode, const GLsizei *count, GLenum type, const void *const*indices, GLsizei primcount, const GLint *basevertex);
{
	glMultiDrawElementsBaseVertexEXT (GLenum mode, const GLsizei *count, GLenum type, const void *const*indices, GLsizei primcount, const GLint *basevertex);
}

void _gl2ext_1_1::_glDrawArraysInstancedEXT (GLenum mode, GLint start, GLsizei count, GLsizei primcount);
{
	glDrawArraysInstancedEXT (GLenum mode, GLint start, GLsizei count, GLsizei primcount);
}

void _gl2ext_1_1::_glDrawElementsInstancedEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei primcount);
{
	glDrawElementsInstancedEXT (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei primcount);
}

void _gl2ext_1_1::_glFramebufferTextureEXT (GLenum target, GLenum attachment, GLuint texture, GLint level);
{
	glFramebufferTextureEXT (GLenum target, GLenum attachment, GLuint texture, GLint level);
}

void _gl2ext_1_1::_glVertexAttribDivisorEXT (GLuint index, GLuint divisor);
{
	glVertexAttribDivisorEXT (GLuint index, GLuint divisor);
}

void _gl2ext_1_1::_glMapBufferRangeEXT (GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
{
	glMapBufferRangeEXT (GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
}

void _gl2ext_1_1::_glFlushMappedBufferRangeEXT (GLenum target, GLintptr offset, GLsizeiptr length);
{
	glFlushMappedBufferRangeEXT (GLenum target, GLintptr offset, GLsizeiptr length);
}

void _gl2ext_1_1::_glMultiDrawArraysEXT (GLenum mode, const GLint *first, const GLsizei *count, GLsizei primcount);
{
	glMultiDrawArraysEXT (GLenum mode, const GLint *first, const GLsizei *count, GLsizei primcount);
}

void _gl2ext_1_1::_glMultiDrawElementsEXT (GLenum mode, const GLsizei *count, GLenum type, const void *const*indices, GLsizei primcount);
{
	glMultiDrawElementsEXT (GLenum mode, const GLsizei *count, GLenum type, const void *const*indices, GLsizei primcount);
}

void _gl2ext_1_1::_glMultiDrawArraysIndirectEXT (GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride);
{
	glMultiDrawArraysIndirectEXT (GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride);
}

void _gl2ext_1_1::_glMultiDrawElementsIndirectEXT (GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride);
{
	glMultiDrawElementsIndirectEXT (GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride);
}

void _gl2ext_1_1::_glRenderbufferStorageMultisampleEXT (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
{
	glRenderbufferStorageMultisampleEXT (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glFramebufferTexture2DMultisampleEXT (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples);
{
	glFramebufferTexture2DMultisampleEXT (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples);
}

void _gl2ext_1_1::_glReadBufferIndexedEXT (GLenum src, GLint index);
{
	glReadBufferIndexedEXT (GLenum src, GLint index);
}

void _gl2ext_1_1::_glDrawBuffersIndexedEXT (GLint n, const GLenum *location, const GLint *indices);
{
	glDrawBuffersIndexedEXT (GLint n, const GLenum *location, const GLint *indices);
}

void _gl2ext_1_1::_glGetIntegeri_vEXT (GLenum target, GLuint index, GLint *data);
{
	glGetIntegeri_vEXT (GLenum target, GLuint index, GLint *data);
}

void _gl2ext_1_1::_glPrimitiveBoundingBoxEXT (GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW);
{
	glPrimitiveBoundingBoxEXT (GLfloat minX, GLfloat minY, GLfloat minZ, GLfloat minW, GLfloat maxX, GLfloat maxY, GLfloat maxZ, GLfloat maxW);
}

GLenum _gl2ext_1_1::_glGetGraphicsResetStatusEXT (void);
{
	return glGetGraphicsResetStatusEXT (void);
}

void _gl2ext_1_1::_glReadnPixelsEXT (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data);
{
	glReadnPixelsEXT (GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data);
}

void _gl2ext_1_1::_glGetnUniformfvEXT (GLuint program, GLint location, GLsizei bufSize, GLfloat *params);
{
	glGetnUniformfvEXT (GLuint program, GLint location, GLsizei bufSize, GLfloat *params);
}

void _gl2ext_1_1::_glGetnUniformivEXT (GLuint program, GLint location, GLsizei bufSize, GLint *params);
{
	glGetnUniformivEXT (GLuint program, GLint location, GLsizei bufSize, GLint *params);
}

void _gl2ext_1_1::_glActiveShaderProgramEXT (GLuint pipeline, GLuint program);
{
	glActiveShaderProgramEXT (GLuint pipeline, GLuint program);
}

void _gl2ext_1_1::_glBindProgramPipelineEXT (GLuint pipeline);
{
	glBindProgramPipelineEXT (GLuint pipeline);
}

GLuint _gl2ext_1_1::_glCreateShaderProgramvEXT (GLenum type, GLsizei count, const GLchar **strings);
{
	return glCreateShaderProgramvEXT (GLenum type, GLsizei count, const GLchar **strings);
}

void _gl2ext_1_1::_glDeleteProgramPipelinesEXT (GLsizei n, const GLuint *pipelines);
{
	glDeleteProgramPipelinesEXT (GLsizei n, const GLuint *pipelines);
}

void _gl2ext_1_1::_glGenProgramPipelinesEXT (GLsizei n, GLuint *pipelines);
{
	glGenProgramPipelinesEXT (GLsizei n, GLuint *pipelines);
}

void _gl2ext_1_1::_glGetProgramPipelineInfoLogEXT (GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
{
	glGetProgramPipelineInfoLogEXT (GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
}

void _gl2ext_1_1::_glGetProgramPipelineivEXT (GLuint pipeline, GLenum pname, GLint *params);
{
	glGetProgramPipelineivEXT (GLuint pipeline, GLenum pname, GLint *params);
}

GLboolean _gl2ext_1_1::_glIsProgramPipelineEXT (GLuint pipeline);
{
	return glIsProgramPipelineEXT (GLuint pipeline);
}

void _gl2ext_1_1::_glProgramParameteriEXT (GLuint program, GLenum pname, GLint value);
{
	glProgramParameteriEXT (GLuint program, GLenum pname, GLint value);
}

void _gl2ext_1_1::_glProgramUniform1fEXT (GLuint program, GLint location, GLfloat v0);
{
	glProgramUniform1fEXT (GLuint program, GLint location, GLfloat v0);
}

void _gl2ext_1_1::_glProgramUniform1fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
{
	glProgramUniform1fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniform1iEXT (GLuint program, GLint location, GLint v0);
{
	glProgramUniform1iEXT (GLuint program, GLint location, GLint v0);
}

void _gl2ext_1_1::_glProgramUniform1ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
{
	glProgramUniform1ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
}

void _gl2ext_1_1::_glProgramUniform2fEXT (GLuint program, GLint location, GLfloat v0, GLfloat v1);
{
	glProgramUniform2fEXT (GLuint program, GLint location, GLfloat v0, GLfloat v1);
}

void _gl2ext_1_1::_glProgramUniform2fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
{
	glProgramUniform2fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniform2iEXT (GLuint program, GLint location, GLint v0, GLint v1);
{
	glProgramUniform2iEXT (GLuint program, GLint location, GLint v0, GLint v1);
}

void _gl2ext_1_1::_glProgramUniform2ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
{
	glProgramUniform2ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
}

void _gl2ext_1_1::_glProgramUniform3fEXT (GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
{
	glProgramUniform3fEXT (GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
}

void _gl2ext_1_1::_glProgramUniform3fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
{
	glProgramUniform3fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniform3iEXT (GLuint program, GLint location, GLint v0, GLint v1, GLint v2);
{
	glProgramUniform3iEXT (GLuint program, GLint location, GLint v0, GLint v1, GLint v2);
}

void _gl2ext_1_1::_glProgramUniform3ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
{
	glProgramUniform3ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
}

void _gl2ext_1_1::_glProgramUniform4fEXT (GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
{
	glProgramUniform4fEXT (GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
}

void _gl2ext_1_1::_glProgramUniform4fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
{
	glProgramUniform4fvEXT (GLuint program, GLint location, GLsizei count, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniform4iEXT (GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
{
	glProgramUniform4iEXT (GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
}

void _gl2ext_1_1::_glProgramUniform4ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
{
	glProgramUniform4ivEXT (GLuint program, GLint location, GLsizei count, const GLint *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix2fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix2fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix3fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix3fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix4fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix4fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glUseProgramStagesEXT (GLuint pipeline, GLbitfield stages, GLuint program);
{
	glUseProgramStagesEXT (GLuint pipeline, GLbitfield stages, GLuint program);
}

void _gl2ext_1_1::_glValidateProgramPipelineEXT (GLuint pipeline);
{
	glValidateProgramPipelineEXT (GLuint pipeline);
}

void _gl2ext_1_1::_glProgramUniform1uiEXT (GLuint program, GLint location, GLuint v0);
{
	glProgramUniform1uiEXT (GLuint program, GLint location, GLuint v0);
}

void _gl2ext_1_1::_glProgramUniform2uiEXT (GLuint program, GLint location, GLuint v0, GLuint v1);
{
	glProgramUniform2uiEXT (GLuint program, GLint location, GLuint v0, GLuint v1);
}

void _gl2ext_1_1::_glProgramUniform3uiEXT (GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2);
{
	glProgramUniform3uiEXT (GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2);
}

void _gl2ext_1_1::_glProgramUniform4uiEXT (GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3);
{
	glProgramUniform4uiEXT (GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3);
}

void _gl2ext_1_1::_glProgramUniform1uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
{
	glProgramUniform1uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
}

void _gl2ext_1_1::_glProgramUniform2uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
{
	glProgramUniform2uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
}

void _gl2ext_1_1::_glProgramUniform3uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
{
	glProgramUniform3uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
}

void _gl2ext_1_1::_glProgramUniform4uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
{
	glProgramUniform4uivEXT (GLuint program, GLint location, GLsizei count, const GLuint *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix2x3fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix2x3fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix3x2fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix3x2fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix2x4fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix2x4fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix4x2fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix4x2fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix3x4fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix3x4fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glProgramUniformMatrix4x3fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glProgramUniformMatrix4x3fvEXT (GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glPatchParameteriEXT (GLenum pname, GLint value);
{
	glPatchParameteriEXT (GLenum pname, GLint value);
}

void _gl2ext_1_1::_glTexParameterIivEXT (GLenum target, GLenum pname, const GLint *params);
{
	glTexParameterIivEXT (GLenum target, GLenum pname, const GLint *params);
}

void _gl2ext_1_1::_glTexParameterIuivEXT (GLenum target, GLenum pname, const GLuint *params);
{
	glTexParameterIuivEXT (GLenum target, GLenum pname, const GLuint *params);
}

void _gl2ext_1_1::_glGetTexParameterIivEXT (GLenum target, GLenum pname, GLint *params);
{
	glGetTexParameterIivEXT (GLenum target, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glGetTexParameterIuivEXT (GLenum target, GLenum pname, GLuint *params);
{
	glGetTexParameterIuivEXT (GLenum target, GLenum pname, GLuint *params);
}

void _gl2ext_1_1::_glSamplerParameterIivEXT (GLuint sampler, GLenum pname, const GLint *param);
{
	glSamplerParameterIivEXT (GLuint sampler, GLenum pname, const GLint *param);
}

void _gl2ext_1_1::_glSamplerParameterIuivEXT (GLuint sampler, GLenum pname, const GLuint *param);
{
	glSamplerParameterIuivEXT (GLuint sampler, GLenum pname, const GLuint *param);
}

void _gl2ext_1_1::_glGetSamplerParameterIivEXT (GLuint sampler, GLenum pname, GLint *params);
{
	glGetSamplerParameterIivEXT (GLuint sampler, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glGetSamplerParameterIuivEXT (GLuint sampler, GLenum pname, GLuint *params);
{
	glGetSamplerParameterIuivEXT (GLuint sampler, GLenum pname, GLuint *params);
}

void _gl2ext_1_1::_glTexBufferEXT (GLenum target, GLenum internalformat, GLuint buffer);
{
	glTexBufferEXT (GLenum target, GLenum internalformat, GLuint buffer);
}

void _gl2ext_1_1::_glTexBufferRangeEXT (GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size);
{
	glTexBufferRangeEXT (GLenum target, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizeiptr size);
}

void _gl2ext_1_1::_glTexStorage1DEXT (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width);
{
	glTexStorage1DEXT (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width);
}

void _gl2ext_1_1::_glTexStorage2DEXT (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
{
	glTexStorage2DEXT (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glTexStorage3DEXT (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
{
	glTexStorage3DEXT (GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
}

void _gl2ext_1_1::_glTextureStorage1DEXT (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width);
{
	glTextureStorage1DEXT (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width);
}

void _gl2ext_1_1::_glTextureStorage2DEXT (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
{
	glTextureStorage2DEXT (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glTextureStorage3DEXT (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
{
	glTextureStorage3DEXT (GLuint texture, GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
}

void _gl2ext_1_1::_glTextureViewEXT (GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers);
{
	glTextureViewEXT (GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers);
}

void _gl2ext_1_1::_glRenderbufferStorageMultisampleIMG (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
{
	glRenderbufferStorageMultisampleIMG (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glFramebufferTexture2DMultisampleIMG (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples);
{
	glFramebufferTexture2DMultisampleIMG (GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLsizei samples);
}

void _gl2ext_1_1::_glBeginPerfQueryINTEL (GLuint queryHandle);
{
	glBeginPerfQueryINTEL (GLuint queryHandle);
}

void _gl2ext_1_1::_glCreatePerfQueryINTEL (GLuint queryId, GLuint *queryHandle);
{
	glCreatePerfQueryINTEL (GLuint queryId, GLuint *queryHandle);
}

void _gl2ext_1_1::_glDeletePerfQueryINTEL (GLuint queryHandle);
{
	glDeletePerfQueryINTEL (GLuint queryHandle);
}

void _gl2ext_1_1::_glEndPerfQueryINTEL (GLuint queryHandle);
{
	glEndPerfQueryINTEL (GLuint queryHandle);
}

void _gl2ext_1_1::_glGetFirstPerfQueryIdINTEL (GLuint *queryId);
{
	glGetFirstPerfQueryIdINTEL (GLuint *queryId);
}

void _gl2ext_1_1::_glGetNextPerfQueryIdINTEL (GLuint queryId, GLuint *nextQueryId);
{
	glGetNextPerfQueryIdINTEL (GLuint queryId, GLuint *nextQueryId);
}

void _gl2ext_1_1::_glGetPerfCounterInfoINTEL (GLuint queryId, GLuint counterId, GLuint counterNameLength, GLchar *counterName, GLuint counterDescLength, GLchar *counterDesc, GLuint *counterOffset, GLuint *counterDataSize, GLuint *counterTypeEnum, GLuint *counterDataTypeEnum, GLuint64 *rawCounterMaxValue);
{
	glGetPerfCounterInfoINTEL (GLuint queryId, GLuint counterId, GLuint counterNameLength, GLchar *counterName, GLuint counterDescLength, GLchar *counterDesc, GLuint *counterOffset, GLuint *counterDataSize, GLuint *counterTypeEnum, GLuint *counterDataTypeEnum, GLuint64 *rawCounterMaxValue);
}

void _gl2ext_1_1::_glGetPerfQueryDataINTEL (GLuint queryHandle, GLuint flags, GLsizei dataSize, GLvoid *data, GLuint *bytesWritten);
{
	glGetPerfQueryDataINTEL (GLuint queryHandle, GLuint flags, GLsizei dataSize, GLvoid *data, GLuint *bytesWritten);
}

void _gl2ext_1_1::_glGetPerfQueryIdByNameINTEL (GLchar *queryName, GLuint *queryId);
{
	glGetPerfQueryIdByNameINTEL (GLchar *queryName, GLuint *queryId);
}

void _gl2ext_1_1::_glGetPerfQueryInfoINTEL (GLuint queryId, GLuint queryNameLength, GLchar *queryName, GLuint *dataSize, GLuint *noCounters, GLuint *noInstances, GLuint *capsMask);
{
	glGetPerfQueryInfoINTEL (GLuint queryId, GLuint queryNameLength, GLchar *queryName, GLuint *dataSize, GLuint *noCounters, GLuint *noInstances, GLuint *capsMask);
}

GLuint64 _gl2ext_1_1::_glGetTextureHandleNV (GLuint texture);
{
	return glGetTextureHandleNV (GLuint texture);
}

GLuint64 _gl2ext_1_1::_glGetTextureSamplerHandleNV (GLuint texture, GLuint sampler);
{
	return glGetTextureSamplerHandleNV (GLuint texture, GLuint sampler);
}

void _gl2ext_1_1::_glMakeTextureHandleResidentNV (GLuint64 handle);
{
	glMakeTextureHandleResidentNV (GLuint64 handle);
}

void _gl2ext_1_1::_glMakeTextureHandleNonResidentNV (GLuint64 handle);
{
	glMakeTextureHandleNonResidentNV (GLuint64 handle);
}

GLuint64 _gl2ext_1_1::_glGetImageHandleNV (GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
{
	return glGetImageHandleNV (GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum format);
}

void _gl2ext_1_1::_glMakeImageHandleResidentNV (GLuint64 handle, GLenum access);
{
	glMakeImageHandleResidentNV (GLuint64 handle, GLenum access);
}

void _gl2ext_1_1::_glMakeImageHandleNonResidentNV (GLuint64 handle);
{
	glMakeImageHandleNonResidentNV (GLuint64 handle);
}

void _gl2ext_1_1::_glUniformHandleui64NV (GLint location, GLuint64 value);
{
	glUniformHandleui64NV (GLint location, GLuint64 value);
}

void _gl2ext_1_1::_glUniformHandleui64vNV (GLint location, GLsizei count, const GLuint64 *value);
{
	glUniformHandleui64vNV (GLint location, GLsizei count, const GLuint64 *value);
}

void _gl2ext_1_1::_glProgramUniformHandleui64NV (GLuint program, GLint location, GLuint64 value);
{
	glProgramUniformHandleui64NV (GLuint program, GLint location, GLuint64 value);
}

void _gl2ext_1_1::_glProgramUniformHandleui64vNV (GLuint program, GLint location, GLsizei count, const GLuint64 *values);
{
	glProgramUniformHandleui64vNV (GLuint program, GLint location, GLsizei count, const GLuint64 *values);
}

GLboolean _gl2ext_1_1::_glIsTextureHandleResidentNV (GLuint64 handle);
{
	return glIsTextureHandleResidentNV (GLuint64 handle);
}

GLboolean _gl2ext_1_1::_glIsImageHandleResidentNV (GLuint64 handle);
{
	return glIsImageHandleResidentNV (GLuint64 handle);
}

void _gl2ext_1_1::_glBlendParameteriNV (GLenum pname, GLint value);
{
	glBlendParameteriNV (GLenum pname, GLint value);
}

void _gl2ext_1_1::_glBlendBarrierNV (void);
{
	glBlendBarrierNV (void);
}

void _gl2ext_1_1::_glBeginConditionalRenderNV (GLuint id, GLenum mode);
{
	glBeginConditionalRenderNV (GLuint id, GLenum mode);
}

void _gl2ext_1_1::_glEndConditionalRenderNV (void);
{
	glEndConditionalRenderNV (void);
}

void _gl2ext_1_1::_glCopyBufferSubDataNV (GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size);
{
	glCopyBufferSubDataNV (GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size);
}

void _gl2ext_1_1::_glCoverageMaskNV (GLboolean mask);
{
	glCoverageMaskNV (GLboolean mask);
}

void _gl2ext_1_1::_glCoverageOperationNV (GLenum operation);
{
	glCoverageOperationNV (GLenum operation);
}

void _gl2ext_1_1::_glDrawBuffersNV (GLsizei n, const GLenum *bufs);
{
	glDrawBuffersNV (GLsizei n, const GLenum *bufs);
}

void _gl2ext_1_1::_glDrawArraysInstancedNV (GLenum mode, GLint first, GLsizei count, GLsizei primcount);
{
	glDrawArraysInstancedNV (GLenum mode, GLint first, GLsizei count, GLsizei primcount);
}

void _gl2ext_1_1::_glDrawElementsInstancedNV (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei primcount);
{
	glDrawElementsInstancedNV (GLenum mode, GLsizei count, GLenum type, const void *indices, GLsizei primcount);
}

void _gl2ext_1_1::_glDeleteFencesNV (GLsizei n, const GLuint *fences);
{
	glDeleteFencesNV (GLsizei n, const GLuint *fences);
}

void _gl2ext_1_1::_glGenFencesNV (GLsizei n, GLuint *fences);
{
	glGenFencesNV (GLsizei n, GLuint *fences);
}

GLboolean _gl2ext_1_1::_glIsFenceNV (GLuint fence);
{
	return glIsFenceNV (GLuint fence);
}

GLboolean _gl2ext_1_1::_glTestFenceNV (GLuint fence);
{
	return glTestFenceNV (GLuint fence);
}

void _gl2ext_1_1::_glGetFenceivNV (GLuint fence, GLenum pname, GLint *params);
{
	glGetFenceivNV (GLuint fence, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glFinishFenceNV (GLuint fence);
{
	glFinishFenceNV (GLuint fence);
}

void _gl2ext_1_1::_glSetFenceNV (GLuint fence, GLenum condition);
{
	glSetFenceNV (GLuint fence, GLenum condition);
}

void _gl2ext_1_1::_glBlitFramebufferNV (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
{
	glBlitFramebufferNV (GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
}

void _gl2ext_1_1::_glRenderbufferStorageMultisampleNV (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
{
	glRenderbufferStorageMultisampleNV (GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glVertexAttribDivisorNV (GLuint index, GLuint divisor);
{
	glVertexAttribDivisorNV (GLuint index, GLuint divisor);
}

void _gl2ext_1_1::_glGetInternalformatSampleivNV (GLenum target, GLenum internalformat, GLsizei samples, GLenum pname, GLsizei bufSize, GLint *params);
{
	glGetInternalformatSampleivNV (GLenum target, GLenum internalformat, GLsizei samples, GLenum pname, GLsizei bufSize, GLint *params);
}

void _gl2ext_1_1::_glUniformMatrix2x3fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glUniformMatrix2x3fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glUniformMatrix3x2fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glUniformMatrix3x2fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glUniformMatrix2x4fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glUniformMatrix2x4fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glUniformMatrix4x2fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glUniformMatrix4x2fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glUniformMatrix3x4fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glUniformMatrix3x4fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

void _gl2ext_1_1::_glUniformMatrix4x3fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
{
	glUniformMatrix4x3fvNV (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
}

GLuint _gl2ext_1_1::_glGenPathsNV (GLsizei range);
{
	return glGenPathsNV (GLsizei range);
}

void _gl2ext_1_1::_glDeletePathsNV (GLuint path, GLsizei range);
{
	glDeletePathsNV (GLuint path, GLsizei range);
}

GLboolean _gl2ext_1_1::_glIsPathNV (GLuint path);
{
	return glIsPathNV (GLuint path);
}

void _gl2ext_1_1::_glPathCommandsNV (GLuint path, GLsizei numCommands, const GLubyte *commands, GLsizei numCoords, GLenum coordType, const void *coords);
{
	glPathCommandsNV (GLuint path, GLsizei numCommands, const GLubyte *commands, GLsizei numCoords, GLenum coordType, const void *coords);
}

void _gl2ext_1_1::_glPathCoordsNV (GLuint path, GLsizei numCoords, GLenum coordType, const void *coords);
{
	glPathCoordsNV (GLuint path, GLsizei numCoords, GLenum coordType, const void *coords);
}

void _gl2ext_1_1::_glPathSubCommandsNV (GLuint path, GLsizei commandStart, GLsizei commandsToDelete, GLsizei numCommands, const GLubyte *commands, GLsizei numCoords, GLenum coordType, const void *coords);
{
	glPathSubCommandsNV (GLuint path, GLsizei commandStart, GLsizei commandsToDelete, GLsizei numCommands, const GLubyte *commands, GLsizei numCoords, GLenum coordType, const void *coords);
}

void _gl2ext_1_1::_glPathSubCoordsNV (GLuint path, GLsizei coordStart, GLsizei numCoords, GLenum coordType, const void *coords);
{
	glPathSubCoordsNV (GLuint path, GLsizei coordStart, GLsizei numCoords, GLenum coordType, const void *coords);
}

void _gl2ext_1_1::_glPathStringNV (GLuint path, GLenum format, GLsizei length, const void *pathString);
{
	glPathStringNV (GLuint path, GLenum format, GLsizei length, const void *pathString);
}

void _gl2ext_1_1::_glPathGlyphsNV (GLuint firstPathName, GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLsizei numGlyphs, GLenum type, const void *charcodes, GLenum handleMissingGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
{
	glPathGlyphsNV (GLuint firstPathName, GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLsizei numGlyphs, GLenum type, const void *charcodes, GLenum handleMissingGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
}

void _gl2ext_1_1::_glPathGlyphRangeNV (GLuint firstPathName, GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLuint firstGlyph, GLsizei numGlyphs, GLenum handleMissingGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
{
	glPathGlyphRangeNV (GLuint firstPathName, GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLuint firstGlyph, GLsizei numGlyphs, GLenum handleMissingGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
}

void _gl2ext_1_1::_glWeightPathsNV (GLuint resultPath, GLsizei numPaths, const GLuint *paths, const GLfloat *weights);
{
	glWeightPathsNV (GLuint resultPath, GLsizei numPaths, const GLuint *paths, const GLfloat *weights);
}

void _gl2ext_1_1::_glCopyPathNV (GLuint resultPath, GLuint srcPath);
{
	glCopyPathNV (GLuint resultPath, GLuint srcPath);
}

void _gl2ext_1_1::_glInterpolatePathsNV (GLuint resultPath, GLuint pathA, GLuint pathB, GLfloat weight);
{
	glInterpolatePathsNV (GLuint resultPath, GLuint pathA, GLuint pathB, GLfloat weight);
}

void _gl2ext_1_1::_glTransformPathNV (GLuint resultPath, GLuint srcPath, GLenum transformType, const GLfloat *transformValues);
{
	glTransformPathNV (GLuint resultPath, GLuint srcPath, GLenum transformType, const GLfloat *transformValues);
}

void _gl2ext_1_1::_glPathParameterivNV (GLuint path, GLenum pname, const GLint *value);
{
	glPathParameterivNV (GLuint path, GLenum pname, const GLint *value);
}

void _gl2ext_1_1::_glPathParameteriNV (GLuint path, GLenum pname, GLint value);
{
	glPathParameteriNV (GLuint path, GLenum pname, GLint value);
}

void _gl2ext_1_1::_glPathParameterfvNV (GLuint path, GLenum pname, const GLfloat *value);
{
	glPathParameterfvNV (GLuint path, GLenum pname, const GLfloat *value);
}

void _gl2ext_1_1::_glPathParameterfNV (GLuint path, GLenum pname, GLfloat value);
{
	glPathParameterfNV (GLuint path, GLenum pname, GLfloat value);
}

void _gl2ext_1_1::_glPathDashArrayNV (GLuint path, GLsizei dashCount, const GLfloat *dashArray);
{
	glPathDashArrayNV (GLuint path, GLsizei dashCount, const GLfloat *dashArray);
}

void _gl2ext_1_1::_glPathStencilFuncNV (GLenum func, GLint ref, GLuint mask);
{
	glPathStencilFuncNV (GLenum func, GLint ref, GLuint mask);
}

void _gl2ext_1_1::_glPathStencilDepthOffsetNV (GLfloat factor, GLfloat units);
{
	glPathStencilDepthOffsetNV (GLfloat factor, GLfloat units);
}

void _gl2ext_1_1::_glStencilFillPathNV (GLuint path, GLenum fillMode, GLuint mask);
{
	glStencilFillPathNV (GLuint path, GLenum fillMode, GLuint mask);
}

void _gl2ext_1_1::_glStencilStrokePathNV (GLuint path, GLint reference, GLuint mask);
{
	glStencilStrokePathNV (GLuint path, GLint reference, GLuint mask);
}

void _gl2ext_1_1::_glStencilFillPathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum fillMode, GLuint mask, GLenum transformType, const GLfloat *transformValues);
{
	glStencilFillPathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum fillMode, GLuint mask, GLenum transformType, const GLfloat *transformValues);
}

void _gl2ext_1_1::_glStencilStrokePathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLint reference, GLuint mask, GLenum transformType, const GLfloat *transformValues);
{
	glStencilStrokePathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLint reference, GLuint mask, GLenum transformType, const GLfloat *transformValues);
}

void _gl2ext_1_1::_glPathCoverDepthFuncNV (GLenum func);
{
	glPathCoverDepthFuncNV (GLenum func);
}

void _gl2ext_1_1::_glCoverFillPathNV (GLuint path, GLenum coverMode);
{
	glCoverFillPathNV (GLuint path, GLenum coverMode);
}

void _gl2ext_1_1::_glCoverStrokePathNV (GLuint path, GLenum coverMode);
{
	glCoverStrokePathNV (GLuint path, GLenum coverMode);
}

void _gl2ext_1_1::_glCoverFillPathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
{
	glCoverFillPathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
}

void _gl2ext_1_1::_glCoverStrokePathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
{
	glCoverStrokePathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
}

void _gl2ext_1_1::_glGetPathParameterivNV (GLuint path, GLenum pname, GLint *value);
{
	glGetPathParameterivNV (GLuint path, GLenum pname, GLint *value);
}

void _gl2ext_1_1::_glGetPathParameterfvNV (GLuint path, GLenum pname, GLfloat *value);
{
	glGetPathParameterfvNV (GLuint path, GLenum pname, GLfloat *value);
}

void _gl2ext_1_1::_glGetPathCommandsNV (GLuint path, GLubyte *commands);
{
	glGetPathCommandsNV (GLuint path, GLubyte *commands);
}

void _gl2ext_1_1::_glGetPathCoordsNV (GLuint path, GLfloat *coords);
{
	glGetPathCoordsNV (GLuint path, GLfloat *coords);
}

void _gl2ext_1_1::_glGetPathDashArrayNV (GLuint path, GLfloat *dashArray);
{
	glGetPathDashArrayNV (GLuint path, GLfloat *dashArray);
}

void _gl2ext_1_1::_glGetPathMetricsNV (GLbitfield metricQueryMask, GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLsizei stride, GLfloat *metrics);
{
	glGetPathMetricsNV (GLbitfield metricQueryMask, GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLsizei stride, GLfloat *metrics);
}

void _gl2ext_1_1::_glGetPathMetricRangeNV (GLbitfield metricQueryMask, GLuint firstPathName, GLsizei numPaths, GLsizei stride, GLfloat *metrics);
{
	glGetPathMetricRangeNV (GLbitfield metricQueryMask, GLuint firstPathName, GLsizei numPaths, GLsizei stride, GLfloat *metrics);
}

void _gl2ext_1_1::_glGetPathSpacingNV (GLenum pathListMode, GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLfloat advanceScale, GLfloat kerningScale, GLenum transformType, GLfloat *returnedSpacing);
{
	glGetPathSpacingNV (GLenum pathListMode, GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLfloat advanceScale, GLfloat kerningScale, GLenum transformType, GLfloat *returnedSpacing);
}

GLboolean _gl2ext_1_1::_glIsPointInFillPathNV (GLuint path, GLuint mask, GLfloat x, GLfloat y);
{
	return glIsPointInFillPathNV (GLuint path, GLuint mask, GLfloat x, GLfloat y);
}

GLboolean _gl2ext_1_1::_glIsPointInStrokePathNV (GLuint path, GLfloat x, GLfloat y);
{
	return glIsPointInStrokePathNV (GLuint path, GLfloat x, GLfloat y);
}

GLfloat _gl2ext_1_1::_glGetPathLengthNV (GLuint path, GLsizei startSegment, GLsizei numSegments);
{
	return glGetPathLengthNV (GLuint path, GLsizei startSegment, GLsizei numSegments);
}

GLboolean _gl2ext_1_1::_glPointAlongPathNV (GLuint path, GLsizei startSegment, GLsizei numSegments, GLfloat distance, GLfloat *x, GLfloat *y, GLfloat *tangentX, GLfloat *tangentY);
{
	return glPointAlongPathNV (GLuint path, GLsizei startSegment, GLsizei numSegments, GLfloat distance, GLfloat *x, GLfloat *y, GLfloat *tangentX, GLfloat *tangentY);
}

void _gl2ext_1_1::_glMatrixLoad3x2fNV (GLenum matrixMode, const GLfloat *m);
{
	glMatrixLoad3x2fNV (GLenum matrixMode, const GLfloat *m);
}

void _gl2ext_1_1::_glMatrixLoad3x3fNV (GLenum matrixMode, const GLfloat *m);
{
	glMatrixLoad3x3fNV (GLenum matrixMode, const GLfloat *m);
}

void _gl2ext_1_1::_glMatrixLoadTranspose3x3fNV (GLenum matrixMode, const GLfloat *m);
{
	glMatrixLoadTranspose3x3fNV (GLenum matrixMode, const GLfloat *m);
}

void _gl2ext_1_1::_glMatrixMult3x2fNV (GLenum matrixMode, const GLfloat *m);
{
	glMatrixMult3x2fNV (GLenum matrixMode, const GLfloat *m);
}

void _gl2ext_1_1::_glMatrixMult3x3fNV (GLenum matrixMode, const GLfloat *m);
{
	glMatrixMult3x3fNV (GLenum matrixMode, const GLfloat *m);
}

void _gl2ext_1_1::_glMatrixMultTranspose3x3fNV (GLenum matrixMode, const GLfloat *m);
{
	glMatrixMultTranspose3x3fNV (GLenum matrixMode, const GLfloat *m);
}

void _gl2ext_1_1::_glStencilThenCoverFillPathNV (GLuint path, GLenum fillMode, GLuint mask, GLenum coverMode);
{
	glStencilThenCoverFillPathNV (GLuint path, GLenum fillMode, GLuint mask, GLenum coverMode);
}

void _gl2ext_1_1::_glStencilThenCoverStrokePathNV (GLuint path, GLint reference, GLuint mask, GLenum coverMode);
{
	glStencilThenCoverStrokePathNV (GLuint path, GLint reference, GLuint mask, GLenum coverMode);
}

void _gl2ext_1_1::_glStencilThenCoverFillPathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum fillMode, GLuint mask, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
{
	glStencilThenCoverFillPathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLenum fillMode, GLuint mask, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
}

void _gl2ext_1_1::_glStencilThenCoverStrokePathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLint reference, GLuint mask, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
{
	glStencilThenCoverStrokePathInstancedNV (GLsizei numPaths, GLenum pathNameType, const void *paths, GLuint pathBase, GLint reference, GLuint mask, GLenum coverMode, GLenum transformType, const GLfloat *transformValues);
}

GLenum _gl2ext_1_1::_glPathGlyphIndexRangeNV (GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLuint pathParameterTemplate, GLfloat emScale, GLuint baseAndCount[2]);
{
	return glPathGlyphIndexRangeNV (GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLuint pathParameterTemplate, GLfloat emScale, GLuint baseAndCount[2]);
}

GLenum _gl2ext_1_1::_glPathGlyphIndexArrayNV (GLuint firstPathName, GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLuint firstGlyphIndex, GLsizei numGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
{
	return glPathGlyphIndexArrayNV (GLuint firstPathName, GLenum fontTarget, const void *fontName, GLbitfield fontStyle, GLuint firstGlyphIndex, GLsizei numGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
}

GLenum _gl2ext_1_1::_glPathMemoryGlyphIndexArrayNV (GLuint firstPathName, GLenum fontTarget, GLsizeiptr fontSize, const void *fontData, GLsizei faceIndex, GLuint firstGlyphIndex, GLsizei numGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
{
	return glPathMemoryGlyphIndexArrayNV (GLuint firstPathName, GLenum fontTarget, GLsizeiptr fontSize, const void *fontData, GLsizei faceIndex, GLuint firstGlyphIndex, GLsizei numGlyphs, GLuint pathParameterTemplate, GLfloat emScale);
}

void _gl2ext_1_1::_glProgramPathFragmentInputGenNV (GLuint program, GLint location, GLenum genMode, GLint components, const GLfloat *coeffs);
{
	glProgramPathFragmentInputGenNV (GLuint program, GLint location, GLenum genMode, GLint components, const GLfloat *coeffs);
}

void _gl2ext_1_1::_glGetProgramResourcefvNV (GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei bufSize, GLsizei *length, GLfloat *params);
{
	glGetProgramResourcefvNV (GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei bufSize, GLsizei *length, GLfloat *params);
}

void _gl2ext_1_1::_glReadBufferNV (GLenum mode);
{
	glReadBufferNV (GLenum mode);
}

void _gl2ext_1_1::_glViewportArrayvNV (GLuint first, GLsizei count, const GLfloat *v);
{
	glViewportArrayvNV (GLuint first, GLsizei count, const GLfloat *v);
}

void _gl2ext_1_1::_glViewportIndexedfNV (GLuint index, GLfloat x, GLfloat y, GLfloat w, GLfloat h);
{
	glViewportIndexedfNV (GLuint index, GLfloat x, GLfloat y, GLfloat w, GLfloat h);
}

void _gl2ext_1_1::_glViewportIndexedfvNV (GLuint index, const GLfloat *v);
{
	glViewportIndexedfvNV (GLuint index, const GLfloat *v);
}

void _gl2ext_1_1::_glScissorArrayvNV (GLuint first, GLsizei count, const GLint *v);
{
	glScissorArrayvNV (GLuint first, GLsizei count, const GLint *v);
}

void _gl2ext_1_1::_glScissorIndexedNV (GLuint index, GLint left, GLint bottom, GLsizei width, GLsizei height);
{
	glScissorIndexedNV (GLuint index, GLint left, GLint bottom, GLsizei width, GLsizei height);
}

void _gl2ext_1_1::_glScissorIndexedvNV (GLuint index, const GLint *v);
{
	glScissorIndexedvNV (GLuint index, const GLint *v);
}

void _gl2ext_1_1::_glDepthRangeArrayfvNV (GLuint first, GLsizei count, const GLfloat *v);
{
	glDepthRangeArrayfvNV (GLuint first, GLsizei count, const GLfloat *v);
}

void _gl2ext_1_1::_glDepthRangeIndexedfNV (GLuint index, GLfloat n, GLfloat f);
{
	glDepthRangeIndexedfNV (GLuint index, GLfloat n, GLfloat f);
}

void _gl2ext_1_1::_glGetFloati_vNV (GLenum target, GLuint index, GLfloat *data);
{
	glGetFloati_vNV (GLenum target, GLuint index, GLfloat *data);
}

void _gl2ext_1_1::_glEnableiNV (GLenum target, GLuint index);
{
	glEnableiNV (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glDisableiNV (GLenum target, GLuint index);
{
	glDisableiNV (GLenum target, GLuint index);
}

GLboolean _gl2ext_1_1::_glIsEnablediNV (GLenum target, GLuint index);
{
	return glIsEnablediNV (GLenum target, GLuint index);
}

void _gl2ext_1_1::_glAlphaFuncQCOM (GLenum func, GLclampf ref);
{
	glAlphaFuncQCOM (GLenum func, GLclampf ref);
}

void _gl2ext_1_1::_glGetDriverControlsQCOM (GLint *num, GLsizei size, GLuint *driverControls);
{
	glGetDriverControlsQCOM (GLint *num, GLsizei size, GLuint *driverControls);
}

void _gl2ext_1_1::_glGetDriverControlStringQCOM (GLuint driverControl, GLsizei bufSize, GLsizei *length, GLchar *driverControlString);
{
	glGetDriverControlStringQCOM (GLuint driverControl, GLsizei bufSize, GLsizei *length, GLchar *driverControlString);
}

void _gl2ext_1_1::_glEnableDriverControlQCOM (GLuint driverControl);
{
	glEnableDriverControlQCOM (GLuint driverControl);
}

void _gl2ext_1_1::_glDisableDriverControlQCOM (GLuint driverControl);
{
	glDisableDriverControlQCOM (GLuint driverControl);
}

void _gl2ext_1_1::_glExtGetTexturesQCOM (GLuint *textures, GLint maxTextures, GLint *numTextures);
{
	glExtGetTexturesQCOM (GLuint *textures, GLint maxTextures, GLint *numTextures);
}

void _gl2ext_1_1::_glExtGetBuffersQCOM (GLuint *buffers, GLint maxBuffers, GLint *numBuffers);
{
	glExtGetBuffersQCOM (GLuint *buffers, GLint maxBuffers, GLint *numBuffers);
}

void _gl2ext_1_1::_glExtGetRenderbuffersQCOM (GLuint *renderbuffers, GLint maxRenderbuffers, GLint *numRenderbuffers);
{
	glExtGetRenderbuffersQCOM (GLuint *renderbuffers, GLint maxRenderbuffers, GLint *numRenderbuffers);
}

void _gl2ext_1_1::_glExtGetFramebuffersQCOM (GLuint *framebuffers, GLint maxFramebuffers, GLint *numFramebuffers);
{
	glExtGetFramebuffersQCOM (GLuint *framebuffers, GLint maxFramebuffers, GLint *numFramebuffers);
}

void _gl2ext_1_1::_glExtGetTexLevelParameterivQCOM (GLuint texture, GLenum face, GLint level, GLenum pname, GLint *params);
{
	glExtGetTexLevelParameterivQCOM (GLuint texture, GLenum face, GLint level, GLenum pname, GLint *params);
}

void _gl2ext_1_1::_glExtTexObjectStateOverrideiQCOM (GLenum target, GLenum pname, GLint param);
{
	glExtTexObjectStateOverrideiQCOM (GLenum target, GLenum pname, GLint param);
}

void _gl2ext_1_1::_glExtGetTexSubImageQCOM (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void *texels);
{
	glExtGetTexSubImageQCOM (GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, void *texels);
}

void _gl2ext_1_1::_glExtGetBufferPointervQCOM (GLenum target, void **params);
{
	glExtGetBufferPointervQCOM (GLenum target, void **params);
}

void _gl2ext_1_1::_glExtGetShadersQCOM (GLuint *shaders, GLint maxShaders, GLint *numShaders);
{
	glExtGetShadersQCOM (GLuint *shaders, GLint maxShaders, GLint *numShaders);
}

void _gl2ext_1_1::_glExtGetProgramsQCOM (GLuint *programs, GLint maxPrograms, GLint *numPrograms);
{
	glExtGetProgramsQCOM (GLuint *programs, GLint maxPrograms, GLint *numPrograms);
}

GLboolean _gl2ext_1_1::_glExtIsProgramBinaryQCOM (GLuint program);
{
	return glExtIsProgramBinaryQCOM (GLuint program);
}

void _gl2ext_1_1::_glExtGetProgramBinarySourceQCOM (GLuint program, GLenum shadertype, GLchar *source, GLint *length);
{
	glExtGetProgramBinarySourceQCOM (GLuint program, GLenum shadertype, GLchar *source, GLint *length);
}

void _gl2ext_1_1::_glStartTilingQCOM (GLuint x, GLuint y, GLuint width, GLuint height, GLbitfield preserveMask);
{
	glStartTilingQCOM (GLuint x, GLuint y, GLuint width, GLuint height, GLbitfield preserveMask);
}

void _gl2ext_1_1::_glEndTilingQCOM (GLbitfield preserveMask);
{
	glEndTilingQCOM (GLbitfield preserveMask);
}


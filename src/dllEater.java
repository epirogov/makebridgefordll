import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by user on 05.11.2017.
 */
public class dllEater {
    String filenameH = "";
    //String nameSpaceCS = "";
    static String prefix ="_";

    public dllEater(String filename){
        filenameH =filename;
    };


    public static String normalizeString(String inp){
        String result ="";
        if (inp!=null){
            result=inp;
            do {
                inp=result;
                result=inp.replaceAll("  ", " ");
            } while ((result.length()!=inp.length()));
            result=result.replaceAll("^ | $","");
        }
        return result;
    }

    public static String reCreate_const(String[] words) throws CharConversionException {
        //#define GL_VERTEX_ATTRIB_ARRAY_ENABLED 0x8622
        String task_type=words[0];
        String result="";
        if (task_type.equals("#define")){
            if (words.length<2) {
                throw new CharConversionException("#define");
            }
            result=String.format("static const int %s%s = %s;", prefix, words[1], words[2]);
        };
        return result;
    };

    public static String reCreate_GL_APICALL_H(String[] words) throws CharConversionException {
        //GL_APICALL void GL_APIENTRY glVertexAttrib1fv (GLuint index, const GLfloat *v);

        String task_type=words[0];
        String result="";
        if (task_type.equals("GL_APICALL")){
            if (words.length<2) {
                throw new CharConversionException("GL_APICALL");
            }

            String signature=prefix+words[3];
            for (int i=4; i<words.length; i++){
                signature+=" "+words[i];
            }
            signature=words[1]+" "+signature;
            result=String.format("static %s", signature);
        };
        return result;
    }

    //Error:(68, 49) java: diamond operator is not supported in -source 1.3
    //(use -source 7 or higher to enable diamond operator)

    //Error:(64, 28) java: generics are not supported in -source 1.3
    //        (use -source 5 or higher to enable generics)


    // in right menu - Open module settings and change from 1.3 old plain java to 9
    public static ArrayList<String> reCreate_GL_APICALL_CPP(String[] words, String nameClass) throws CharConversionException {
        //GL_APICALL void GL_APIENTRY glVertexAttrib1fv (GLuint index, const GLfloat *v);

        String task_type=words[0];
        ArrayList<String> result= new ArrayList<String>();
        //String re="";
        if (task_type.equals("GL_APICALL")){
            if (words.length<3) {
                throw new CharConversionException("GL_APICALL");
            }

            String signature_dll=words[3];
            String signature_cpp=prefix+words[3];
            for (int i=4; i<words.length; i++){
                signature_cpp+=" "+words[i];
                signature_dll+=" "+words[i];
            }
            if (!nameClass.isEmpty()) signature_cpp=nameClass+"::"+signature_cpp;
            signature_cpp=words[1]+" "+signature_cpp;

            //re=String.format("static %s", signature_cpp);


            result.add(signature_cpp);
            result.add("{");

            result.add("\t"+(words[1].equals("void")?"":"return ")+signature_dll);
            result.add("}");
        };

        return result;
    }

/*
    // Returns a + b
    DLLLOADERTEST_API double MyAdd(double a, double b)
    {
        return Add(a, b);
    }
*/

    public void generate(String name){
        try {
            FileWriter fw_h = new FileWriter(name+".h");
            FileWriter fw_cpp = new FileWriter(name+".cpp");

            fw_h.write(String.format("class %s\n", name));
            fw_h.write("{\n");
            fw_h.write("\tpublic:\n");
            fw_h.write(String.format("\t\t%s(void);\n", name));


            int line_count=0;

            FileReader fr = new FileReader(filenameH);
            Scanner input = new Scanner(fr);
            while (input.hasNextLine()){
                String task=input.nextLine();
                String result_H="";
                ArrayList<String> result_CPP= new ArrayList<>();
                String str = normalizeString(task);
                String[] words = str.split(" ");
                try {
                    result_H+=reCreate_const(words);
                    result_H+= reCreate_GL_APICALL_H(words);

                    result_CPP.addAll(reCreate_GL_APICALL_CPP(words, name));

                } catch (CharConversionException e){
                    System.out.format("Error of % in line %d of h file %s\n", e.getMessage(), line_count, filenameH);
                }

                if (!result_H.equals("")) fw_h.write(String.format("\t\t"+result_H+"\n"));
                if (result_CPP.size()>0) {
                    for (int i=0; i<result_CPP.size(); i++) {
                        fw_cpp.write(String.format("%s\n", result_CPP.get(i)));
                    }
                    fw_cpp.write("\n");
                }
                line_count++;
            }

            fw_h.write("}\n");

            fw_h.close();
            fw_cpp.close();
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }
}
